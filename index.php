<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'meta.php'; ?>
    <meta property="og:title" content="Login | adminMypet" />
    <title>Login | adminMypet</title>
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div >
    <h1>Login</h1>
    <p>
        Please enter your ID and password to login.
    </p>
    <div class="clear"></div>   
    <form  class="tele-form" action="utilities/loginFunction.php" method="POST">
        <input class="clean tele-input" type="text" placeholder="Username" id="username" name="username" required >
        <div class="clear"></div>
        <input class="clean tele-input" type="password" placeholder="Password" id="password" name="password" required>
        <div class="clear"></div>
        <?php
            if(isset($_COOKIE['remember-mypetslib']) && $_COOKIE['remember-oilxag'] == 1)
            {
            ?>
                <div class="left-check"><input checked type="checkbox" class="checkbox-remember-me" name="remember-me"> <p class="remember-me-p"> Remember Me</p></div>
            <?php
            }
            else
            {
            ?>
                <div class="left-check"><input type="checkbox" class="checkbox-remember-me" name="remember-me">Remember Me</div>
            <?php
            }
        ?>
        <button class="clean red-btn"name="loginButton">Login</button>
        <div class="clear"></div>
        <div"><a class="open-forgot forgot-password-a" style="text-decoration:none" href="https://www.w3schools.com">Forgot Password</a></div>
        <div class="clear"></div>
    </form>     
</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Login!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no user with this username ! Please login again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "User registration failed!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Incorrect password! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>