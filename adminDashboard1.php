<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
//require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
<?php include 'meta.php'; ?>
<meta property="og:title" content="Login | adminMypet" />
<title>Login | adminMypet</title>
<?php include 'css.php'; ?>
<style>

.input50-div {
    width: 48%;
    float: left;
}

.second-input50 {
  margin-left: 4%;
}

.input100-div {
    width: 100%;
    float: left;
}

</style>
</head>

<body class="body">

<?php
echo "Admin Dashboard";
?>

    <!-- Seller SignUp Modal -->
    <div id="seller-signup-modal">
    <!-- Modal content -->
        <div>
          <h1 ><?php echo "Add Seller" ?></h1>

            <!-- <form> -->
            <form action="utilities/sellerRegisterFunction.php" method="POST">
                <div class="input50-div">
                    <p>Company Name</p>
                    <input  type="text" placeholder="<?php echo "Company Name*" ?>" id="register_company_name" name="register_company_name" required>       
                </div>
                
                <div class="input50-div second-input50" >
                    <p>Company Slug(For URL)*</p>
                    <input  type="url" placeholder="<?php echo "Company Slug(For URL)*" ?>" id="register_slug" name="register_slug" required>
                </div>

                <div class="input50-div">
                    <p>Company Registration No.</p>
                    <input type="text" placeholder="<?php echo"Company Registration No." ?>" id="register_registration_no" name="register_registration_no" required>
                </div>

                <div class="input50-div second-input50">
                    <p>Company Contact</p>
                    <input type="text" placeholder="<?php echo "Company Contact" ?>" id="register_phone" name="register_phone" required>
                </div>
                
                <div class="input100-div">
                    <p>Company Address</p>
                    <input  type="text" placeholder="<?php echo "Company Address" ?>" id="register_address" name="register_address" required>
                </div>

                <div class="input50-div">
                    <p>Company State</p>      
                    <input  type="text" placeholder="<?php echo "Company State*" ?>" id="register_state" name="register_state">       
                </div>

                <div class="input50-div second-input50">
                    <p>Account State</p>
                    <input  type="text" placeholder="<?php echo "Company State*" ?>" id="register_state" name="register_state">
                    <!-- <select  type="text" placeholder="<?php echo "Account State*" ?>" id="register_account_status" name="register_account_status" required>
                    <option value="" name=" ">PLEASE SELECT ACCOUNT TYPE</option>
                    <option value="active" name="active">Active</option>
                    <option value="inactive" name="inactive">Inactive</option>
                    </select> -->
                </div>

                <div class="input50-div">
                    <p>Contact Person Name</p>
                    <input  type="text" placeholder="<?php echo "Contact Person Name*" ?>" id="register_contact_person" name="register_contact_person" required>
                </div>

                <div class="input50-div second-input50">
                    <p>Contact Person Contact No</p>
                    <input  type="text" placeholder="<?php echo "Contact Person Contact No.*" ?>" id="register_contact_person_no" name="register_contact_person_no" required>
                </div>

                <div class="input50-div">
                    <p>Years of Experience</p>
                    <input type="text" placeholder="<?php echo"Years of Experience" ?>" id="register_experience" name="register_experience" required>
                </div>

                <div class="input50-div second-input50">
                    <p>Cert</p>
                    <input type="text" placeholder="<?php echo "Cert" ?>" id="register_cert" name="register_cert" required>
                </div>

                <div class="input100-div">
                    <p>Services</p>
                    <input  type="text" placeholder="<?php echo "Services" ?>" id="register_services" name="register_services" required>
                </div>

                <div class="input100-div">
                    <p>Type of Breed</p>      
                    <input  type="text" placeholder="<?php echo "Type of Breed" ?>" id="register_breed_type" name="register_breed_type">       
                </div>

                <div class="input100-div">
                    <p>Other Info</p>
                    <input  type="text" placeholder="<?php echo "Other Info" ?>" id="register_other_info" name="register_other_info" required>
                </div>
                
                <div class="input100-div">
                    <p>Upload Company Logo</p>      
                    <input  type="text" placeholder="<?php echo "Upload Company Logo" ?>" id="register_logo" name="register_logo">       
                </div>

                <div class="clear"></div>
                </br>
                
                <button name="loginButton"><?php echo "Sign Up" ?></button>

            </form>

        </div>
    </div>
</body>
</html>