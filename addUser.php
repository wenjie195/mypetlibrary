<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include 'meta.php'; ?>
    <meta property="og:title" content="Login | adminMypet" />
    <title>Login | adminMypet</title>
    <?php include 'css.php'; ?>
</head>

<body class="body">
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<!-- Signup Modal -->
<div id="signup-modal">
    <!-- Modal content -->
    <div>
    <h1 class="h1-title">Sign Up </h1>
    <button name="TagButton"><?php echo "Login With Facebook" ?></button>
    <div class="clear"></div>
    <p>Email or phone number must key in either one or both</p>
    <!-- <form> -->
    <form action="utilities/registerFunction.php" method="POST">
        <input  type="text" placeholder="<?php echo "Name" ?>" id="register_username" name="register_username" required>
        <div class="clear"></div>
        </br>
        <input  type="email" placeholder="<?php echo "Email Address" ?>" id="register_email_user" name="register_email_user" required>
        <div class="clear"></div>
        </br>
        <button name="verifyButton"><?php echo "Verify Email" ?></button>
        <div class="clear"></div>
        </br>
        <input class="clean de-input" type="password" placeholder="<?php echo"Password" ?>" id="register_password" name="register_password" required>
        <div class="clear"></div>
        </br>
        <input class="clean de-input" type="password" placeholder="<?php echo "Retype Password" ?>" id="register_retype_password" name="register_retype_password" required>
        <div class="clear"></div>
        </br>
        <input  type="text" placeholder="<?php echo "Phone Number" ?>" id="register_phone" name="register_phone" required>
        <div class="clear"></div>
        </br>
        <button name="TagButton"><?php echo "Request Tag" ?></button>
        <div class="clear"></div>
        </br>   
        <input  type="text" placeholder="<?php echo "TAC" ?>" id="register_tac" name="register_tac">       
        <div class="clear"></div>
        </br>
        <p>By clicking "Sign Up", I agree to Mypetslibrary Privacy Policy and Terms and Condition</p>
        <button name="loginButton"><?php echo "Sign Up" ?></button>
        <div class="clear"></div>
        <div"><a class="open-forgot forgot-password-a" style="text-decoration:none" href="https://www.w3schools.com">Forgot Password</a></div>
        <div class="clear"></div>
        <div><a class="open-forgot forgot-password-a" style="text-decoration:none" href="https://www.w3schools.com">Login</a></div>
        <div class="clear"></div>
    </form>

  </div>
</div>
<?php include 'js.php'; ?>
<?php

if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully registered new user!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Registration of new user failed!";
        } 
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>