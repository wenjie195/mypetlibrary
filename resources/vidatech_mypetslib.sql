-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2020 at 02:34 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_mypetslib`
--

-- --------------------------------------------------------

--
-- Table structure for table `seller`
--

CREATE TABLE `seller` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `company_name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `registration_no` varchar(200) NOT NULL,
  `phone_no` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `state` varchar(200) NOT NULL,
  `account_status` varchar(255) NOT NULL,
  `contact_person` varchar(200) NOT NULL,
  `contact_person_no` varchar(200) NOT NULL,
  `experience` varchar(200) NOT NULL,
  `cert` varchar(200) NOT NULL,
  `services` varchar(200) NOT NULL,
  `breed_type` varchar(200) NOT NULL,
  `other_info` varchar(255) NOT NULL,
  `company_logo` varchar(200) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `tac` varchar(200) DEFAULT NULL,
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `email_verification_code` varchar(10) DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT 1,
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT 0,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `tac`, `user_type`, `email_verification_code`, `is_email_verified`, `is_phone_verified`, `date_created`, `date_updated`) VALUES
(1, 'c93acb097ab7b53145b1467ae68ab07b', 'admin', 'admin@gmail.com', 'bdca8974484bdf986d1cf2393f3e5d98151669e34a5ec3ab6ed81455f47d031c', '5bf9099f03c268ea28b18b09901c5d1c9a21797a', '0101111111', 'admin', 0, NULL, 1, 0, '2020-03-10 04:08:31', '2020-03-10 04:08:48'),
(2, 'fd1427e01f65783447c12b2e1923caf9', 'user', 'user@gmail.com', 'd4a995ca71613ce44640adacc8f91f8704419b51915369545e31de0fb7a92991', '363c15b7fb6a95c243652bba557023bc4733f069', '0111111111', 'user', 1, NULL, 1, 0, '2020-03-10 04:15:44', '2020-03-10 04:15:44'),
(3, '8b09befd29d6d2777e36642291e8fb8d', 'user1', 'user1@gmail.com', '3359b1c9a244d44e548a2860e61c9ec3362579a1c8dd3c7585153444aeb9b330', 'e1fa9d10078ef4855cee68c27972375561d26b17', '0104691111', 'xxxx', 1, NULL, 1, 0, '2020-03-18 04:56:03', '2020-03-18 04:56:03'),
(4, 'e4f82e615d361a8a34efed9f81deed9b', 'user3', 'user3@gmail.com', '27fe9add229d528db453ea4980c31db575302f5911f3646e1ef30398956d3bb5', 'd4be10c3b8637ab254d9baa8abfe1354f67449ed', '104698362', 'user3', 1, NULL, 1, 0, '2020-03-18 07:12:25', '2020-03-18 07:12:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `seller`
--
ALTER TABLE `seller`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `seller`
--
ALTER TABLE `seller`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
